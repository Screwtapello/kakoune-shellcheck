ShellCheck integration for Kakoune
==================================

[ShellCheck] is a static analysis tool for the POSIX shell.
It can analyze a shell-script
and report common bugs and portability problems,
helping you to write more portable, reliable code.

[ShellCheck]: https://www.shellcheck.net/

Features
========

  - Configures Kakoune to use ShellCheck
    when you run `:lint` while editing a shell script.
  - Configures Kakoune to use ShellCheck
    to analyze `%sh{}` blocks
    when you run `:lint` while editing a Kakoune script.

Requirements
============

  - You must have the `shellcheck` command installed locally,
    and available on `$PATH`
      - It's available in most Unixy package managers already
      - For more information,
        see the Installing section of the [ShellCheck README].
  - You must have a version of Kakoune that includes commit af7091f
      - This commit landed on 2020-02-16,
        so if your Kakoune was released after that date,
        you're probably good.
        
[ShellCheck README]: https://github.com/koalaman/shellcheck#installing

Installation
============

 1. Make the directory `~/.config/kak/autoload/`
    if it doesn't already exist.

        mkdir -p ~/.config/kak/autoload

 2. If it didn't already exist,
    create a symlink inside that directory
    pointing to Kakoune's default autoload directory
    so it Kakoune will still be able to find
    its default configuration.
    You can find Kakoune's runtime autoload directory
    by typing

        :echo %val{runtime}/autoload

    inside Kakoune.
 3. Put a copy of `shellcheck.kak` inside
    the new autoload directory,
    such as by checking out this git repository:

        cd ~/.config/kak/autoload/
        git clone https://gitlab.com/Screwtapello/kakoune-shellcheck.git

Configuration
=============

This plugin provides no configuration options.

Commands
========

This plugin provides no custom commands.

Usage
=====

After the plugin is installed,
start Kakoune and check the `*debug*` buffer.
If you see a message like this:

> shellcheck not found in PATH

...then the plugin could not find the `shellcheck` executable.
Make sure you can run `shellcheck --version` from a terminal,
then try Kakoune again.

Once `shellcheck` is available,
and you're editing a shell script
or a Kakoune script,
you can use the `:lint` command
(and related commands)
as usual:

  - Run `:lint`, and a message will be displayed
    counting the number of errors and warnings detected.
  - Use `:lint-next-message` and `:lint-previous-message`
    to navigate between them.
  - Alternatively, use `:buffer *lint-output*`
    to see all the lint messages in a list.
  - Use `:lint-enable` to display an icon in the left-hand column
    of each line with a problem,
    and move the cursor onto that line to display the message.
      - Use `:lint-disable` to hide lint messages again.
